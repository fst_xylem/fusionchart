import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import * as FusionCharts from 'fusioncharts';
import * as Charts from 'fusioncharts/fusioncharts.charts';
import * as Widgets from 'fusioncharts/fusioncharts.widgets';
import * as FintTheme from 'fusioncharts/themes/fusioncharts.theme.fint';

import { FusionChartsModule } from 'angular4-fusioncharts';


import { AppComponent } from './app.component';
import { BarChartComponent } from './components/bar-chart/bar-chart.component';
import { ZoomlineComponent } from './components/zoomline/zoomline.component';
import { AngulargaugeComponent } from './components/angulargauge/angulargauge.component';

FusionChartsModule.fcRoot(FusionCharts, Charts, FintTheme, Widgets);


const appRoutes: Routes = [
  {
    path: 'barChart',
    component: BarChartComponent
  },
  {
    path: 'zoomlineChart',
    component: ZoomlineComponent
  },
  {
    path: 'angulargauge',
    component: AngulargaugeComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    BarChartComponent,
    ZoomlineComponent,
    AngulargaugeComponent
  ],
  imports: [
    BrowserModule,
    FusionChartsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
