import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngulargaugeComponent } from './angulargauge.component';

describe('AngulargaugeComponent', () => {
  let component: AngulargaugeComponent;
  let fixture: ComponentFixture<AngulargaugeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AngulargaugeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AngulargaugeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
