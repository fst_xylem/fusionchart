import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-angulargauge',
  templateUrl: './angulargauge.component.html',
  styleUrls: ['./angulargauge.component.css']
})
export class AngulargaugeComponent {
  width = '100%';
  height = 450;
  type = 'angulargauge';
  dataFormat = 'json';
  dataSource;
  constructor() {
    this.dataSource = {
      'chart': {
        'caption': 'Customer Satisfaction Score',
        'subcaption': 'Last week',
        'lowerLimit': '0',
        'upperLimit': '100',
        'theme': 'fint'
    },
    'colorRange': {
        'color': [
            {
                'minValue': '0',
                'maxValue': '50',
                'code': '#e44a00'
            },
            {
                'minValue': '50',
                'maxValue': '75',
                'code': '#f8bd19'
            },
            {
                'minValue': '75',
                'maxValue': '100',
                'code': '#6baa01'
            }
        ]
    },
    'dials': {
        'dial': [{
            'value': '67'
        }]
    }
    };
   }
}
